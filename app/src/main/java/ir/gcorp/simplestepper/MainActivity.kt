package ir.gcorp.simplestepper

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Build
import android.os.Bundle
import android.support.v4.BuildConfig
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.fitness.Fitness
import com.google.android.gms.fitness.FitnessOptions
import com.google.android.gms.fitness.data.DataType
import com.google.android.gms.fitness.data.Field
import ir.gcorp.simplestepper.ui.Fragment_Settings
import ir.gcorp.simplestepper.util.API26Wrapper
import ir.gcorp.simplestepper.util.Logger
import ir.gcorp.simplestepper.util.Util
import kotlinx.android.synthetic.main.activity_main.*
import java.text.NumberFormat
import java.util.*


/**
 * This sample demonstrates combining the Recording API and History API of the Google Fit platform
 * to record steps, and display the daily current step count. It also demonstrates how to
 * authenticate a user with Google Play Services.
 */
class MainActivity : AppCompatActivity(), SensorEventListener {


    private var todayOffset: Int = 0
    private var total_start: Int = 0
    private var goal: Int = 0
    private var since_boot: Int = 0
    private var total_days: Int = 0
    val formatter = NumberFormat.getInstance(Locale.getDefault())
    private val showSteps = true
    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {

    }

    override fun onSensorChanged(event: SensorEvent) {
        if (BuildConfig.DEBUG)
            Logger.log(
                    "UI - sensorChanged | todayOffset: " + todayOffset + " since boot: " +
                            event.values[0])
        if (event.values[0] > Integer.MAX_VALUE || event.values[0] == 0f) {
            return
        }
        if (todayOffset == Integer.MIN_VALUE) {
            // no values for today
            // we dont know when the reboot was, so set todays steps to 0 by
            // initializing them with -STEPS_SINCE_BOOT
            todayOffset = -event.values[0].toInt()
            val db = Database.getInstance(this)
            db.insertNewDay(Util.getToday(), event.values[0].toInt())
            db.close()
        }
        since_boot = event.values[0].toInt()
        title_text_view.text = todayOffset.toString()
//        updatePie();
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //subscribe()
        /*   val fitnessOptions = FitnessOptions.builder()
                   .addDataType(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                   .addDataType(DataType.TYPE_STEP_COUNT_DELTA)
                   .build()
           if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(this), fitnessOptions)) {
               GoogleSignIn.requestPermissions(
                       this,
                       REQUEST_OAUTH_REQUEST_CODE,
                       GoogleSignIn.getLastSignedInAccount(this),
                       fitnessOptions)
           } else {
               subscribe()
           }*/
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_OAUTH_REQUEST_CODE) {
                subscribe()
            }
        }
    }

    /** Records step data by requesting a subscription to background step data.  */
    fun subscribe() {
        // To create a subscription, invoke the Recording API. As soon as the subscription is
        // active, fitness data will start recording.
//    Fitness.getRecordingClient(this, GoogleSignIn.getLastSignedInAccount(this)!!)
//            .subscribe(DataType.TYPE_STEP_COUNT_CUMULATIVE)
//            .addOnCompleteListener { task ->
//              if (task.isSuccessful) {
//                Log.i(TAG, "Successfully subscribed!")
//              } else {
//                Log.w(TAG, "There was a problem subscribing.", task.exception)
//                Log.e("StepCounter",task.exception.toString())
//                task.exception?.printStackTrace()
//              }
//            }

        if (Build.VERSION.SDK_INT >= 26) {
            API26Wrapper.startForegroundService(this, Intent(this, SensorListener::class.java))
        } else {
            startService(Intent(this, SensorListener::class.java))
        }

    }

    /**
     * Reads the current daily step total, computed from midnight of the current day on the device's
     * current timezone.
     */
    private fun readData() {
        Fitness.getHistoryClient(this, GoogleSignIn.getLastSignedInAccount(this)!!)
                .readDailyTotal(DataType.TYPE_STEP_COUNT_DELTA)
                .addOnSuccessListener { dataSet ->
                    val total = (if (dataSet.isEmpty)
                        0
                    else
                        dataSet.dataPoints[0].getValue(Field.FIELD_STEPS).asInt()).toLong()
                    Log.i(TAG, "Total steps: $total")
                }
                .addOnFailureListener { e -> Log.w(TAG, "There was a problem getting the step count.", e) }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the main; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        /*  val id = item.itemId
          if (id == R.id.action_read_data) {
              readData()
              return true
          }
          return super.onOptionsItemSelected(item)*/
        return true
    }

    /** Initializes a custom log class that outputs both to in-app targets and logcat.  */


    public override fun onResume() {
        super.onResume()

        val db = Database.getInstance(this)

        if (BuildConfig.DEBUG) db.logState()
        // read todays offset
        todayOffset = db.getSteps(Util.getToday())

        val prefs = getSharedPreferences("pedometer", Context.MODE_PRIVATE)

        goal = prefs.getInt("goal", 1000)
        since_boot = db.currentSteps
        val pauseDifference = since_boot - prefs.getInt("pauseCount", since_boot)

        // register a sensorlistener to live update the UI if a step is taken
        val sm = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        val sensor = sm.getDefaultSensor(Sensor.TYPE_STEP_COUNTER)

        val pm = packageManager
        if (pm.hasSystemFeature(PackageManager.FEATURE_SENSOR_STEP_COUNTER)) {
            Toast.makeText(this,"OK",Toast.LENGTH_LONG).show()
            sm.registerListener(this, sensor, SensorManager.SENSOR_DELAY_UI, 0)

        }else{
            AlertDialog.Builder(this).setTitle(R.string.no_sensor)
                    .setMessage(R.string.no_sensor_explain)
                    .setOnDismissListener { this.finish() }.setNeutralButton(android.R.string.ok) { dialogInterface, i -> dialogInterface.dismiss() }.create().show()

        }
        since_boot -= pauseDifference

        total_start = db.totalWithoutToday
        total_days = db.days

        db.close()

        stepsDistanceChanged()
    }

    /**
     * Call this method if the Fragment should update the "steps"/"km" text in
     * the pie graph as well as the pie and the bars graphs.
     */
    private fun stepsDistanceChanged() {

        val db = Database.getInstance(this);
        val last = db.getLastEntries(8);
        db.close();

        var i = last.size - 1
        while (i > 0) {

            var current = last[i]
            var steps = current.second
            if (steps > 0) {

                title_text_view.text = "steps : $steps"


            }
            i--
        }
    }

    companion object {

        val TAG = "StepCounter"
        private val REQUEST_OAUTH_REQUEST_CODE = 0x1001
    }
}
